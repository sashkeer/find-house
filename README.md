# 🚀 Welcome to your new awesome project!

This project has been created using **webpack-cli**, you can now run

```
npm run build
```

or

```
yarn build
```

to bundle your application

for db creation
npx prisma migrate dev --name init


docker run --name postgres -p 5432:5432 -e POSTGRES_PASSWORD=20012001 -d postgres:15.1


### Aim of the project

twice a day request for new flats and make the reports of the best choices