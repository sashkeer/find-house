import * as Handlebars from "handlebars";
import * as fs from "fs";
import * as puppeteer from "puppeteer";
import moment = require("moment");
import { wait } from '../utils';

Handlebars.registerHelper("json", function (context) {
  return JSON.stringify(context);
});

Handlebars.registerHelper("date", function (context) {
  return moment(context).format('YYYY-MM-DD');
});

export class Report<T> {
  constructor(private template: string) {}

  public async generate(params: T, path: string): Promise<string> {
    return new Promise((resolve, reject) => {
      fs.readFile(this.template, (err, data) => {
        if (err) reject(err);

        const hbsTemplate = Handlebars.compile(data.toString(), {
          noEscape: true,
        });
        const advertReport = hbsTemplate(params);
        fs.writeFile(path + ".html", advertReport, { flag: "w" }, (err) => {
          if (err) console.log(err);
          resolve(advertReport);
        });
      });
    });
  }

  public async generatePdf(params: T, path: string): Promise<string> {
    const filepath = path + ".pdf";
    const reportTemplate = fs.readFileSync(this.template, {
      encoding: "utf-8",
    });

    const hbsTemplate = Handlebars.compile(reportTemplate, {
      noEscape: true,
    });
    const advertReport = hbsTemplate(params);

    const browser = await puppeteer.launch({
      args: ["--no-sandbox"],
      headless: true,
    });
    const page = await browser.newPage();

    await page.setContent(advertReport, {
      waitUntil: ["load", "networkidle0"],
    });

    await page.pdf({ path: filepath, format: "A4" });

    await browser.close();

    return filepath;
  }
}
