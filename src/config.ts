import { v4 as uuidv4 } from 'uuid';
import * as path from 'path';

export const runId = uuidv4();
export const reportsPath = path.resolve(__dirname, '../reports');