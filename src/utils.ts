import { Prisma } from "@prisma/client";

export const logActivity = (...args: any[]) =>
  console.log(`[${new Date().toDateString()}]`, ...args);

export const wait = (ms: number) =>
  new Promise((resolve) => setTimeout(resolve, ms));

export const joinQuery = (params: Prisma.JsonValue) =>
  typeof params === "object" && !(params instanceof Array)
    ? Object.keys(params)
        .map((key) => `${key}=${params[key]}`)
        .join("&")
    : {};
