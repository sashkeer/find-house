import { MyhomeAdvertExtractor } from "./web-scraper/extractor/myhome-advert.extractor";
import { PrismaClient } from "@prisma/client";
import { PuppeteerCrawler } from "./web-scraper/crawler/puppeteer.crawler";
import { MyhomeAdvertStorage } from "./web-scraper/storage/myhome-advert.storage";
import { NextMyhomeProvider } from "./web-scraper/next-page/next-myhome.provider";
import { Scraper } from "./web-scraper/scraper/scraper";
import { joinQuery } from "./utils";
import { MyhomeAdvertInfoExtractor } from "./web-scraper/extractor/myhome-advert-info.extractor";
import { MyhomePageController } from "./web-scraper/crawler/page.controller/myhome-page.controller";
import { GmailNotifier } from "./web-scraper/notifier/gmail.notifier";
import * as fs from "fs";
import { Report } from "./reports/report";
import * as path from "path";
import { reportsPath, runId } from './config';
import { MyhomeAdvertInfoStorage } from './web-scraper/storage/myhome-advert-info.storage';

const prisma = new PrismaClient();

const baseUrl = "https://www.myhome.ge/ru/s/Продается-квартира-Тбилиси";

interface AggregatedAdvert {
  price: number;
  area: string;
  title: string;
  floor: string;
  room: string;
  url: string;
  description: string;
  phones: string;
  photos: string[];
  amenities: string[];
  views: number;
  is_new: boolean;
  district: string;
  city: string;
  breadcrumbs: string[];
  publish_date: Date;
  hasten: boolean;
  discount: boolean;
}

const reportName = `${new Date().toDateString()}-advert-list`;
const reportFilePath = path.resolve(reportsPath, reportName);

const interestingAdvertsQuery = `
    select
      *
    from
      flats
    where
      true
      and title ~* 'ново'
      and not (amenities @> '{Хрущевка}')
      and amenities && '{Новый Ремонт,Белый Каркас}'
      and cardinality(photos) > 5
      and price > 15000
      and is_new = true
      and room > 1
    order by
      publish_date desc,
      views desc,
      price asc
    limit 10
`;

(async () => {
  console.time("scraping");

  const searchCities = await prisma.searchParams.findMany({ where: { active: true }});

  for (const searchCity of searchCities) {
    const realtyUrl = `${baseUrl}?${joinQuery(searchCity.parameters)}`;
    const scraper = new Scraper(
      new MyhomeAdvertExtractor(),
      new PuppeteerCrawler(),
      new MyhomeAdvertStorage(prisma),
      new NextMyhomeProvider()
    );

    await scraper.start([realtyUrl]);
  }

  const adverts = await prisma.advert.findMany({
    where: {
      id: 129,
      run_id: runId,
      advert_info: {
        data_id: null,
      },
    },
    include: {
      advert_info: true,
    },
  });

  console.log(adverts);

  const scraperInfo = new Scraper(
    new MyhomeAdvertInfoExtractor(),
    new PuppeteerCrawler(new MyhomePageController()),
    new MyhomeAdvertInfoStorage(prisma)
  );

  await scraperInfo.start(adverts.map((advert) => advert.url));

  /**
   * Generating reports
   */

  const interestingAdverts: AggregatedAdvert[] = await prisma.$queryRaw`${interestingAdvertsQuery}`;

  if (!interestingAdverts.length) {
    console.warn('No new adverts found');
    return;
  }

  const report = new Report(
    path.resolve(__dirname, `../src/reports/myhome-advert-list.hbs`)
  );

  const filepath = await report.generatePdf(
    {
      caption: "Новые квартиры",
      statistics: {
        flatsByType: convertAgedToBarChart(await getAgedFlatStatistics()),
        full: await getFullStatistics(),
      },
      adverts: interestingAdverts.map((advert) => ({
        ...advert,
        price: advert.price?.toLocaleString("en-US"),
      })),
    },
    reportFilePath
  );

  const notifier = new GmailNotifier();

  const fileBuffer = fs.readFileSync(filepath);

  await notifier.notify(
    "My home report",
    "Daily report observing new flats from Georgian web sites",
    [{ name: filepath.split('/').pop(), buffer: fileBuffer }]
  );

  console.timeEnd("scraping");
})();

interface AgedStatistics {
  count: number;
  title: string;
}

function convertAgedToBarChart(agedStatistics: AgedStatistics[]) {
  return {
    labels: agedStatistics.map((stat) => stat.title),
    datasets: [{
      label: '# of flats',
      data: agedStatistics.map((stat) => stat.count),
      borderWidth: 1
    }]
  };
}

async function getAgedFlatStatistics(): Promise<AgedStatistics[]> {
  return await prisma.$queryRaw`
    select
      count(title)::int count, title
    from
      flats
    where run_id = '${runId}'
    group by
      title
  `;
}

interface FullStatistics {
  count: number;
  city_id: string;
  floor: string;
  avg_price_per_meter: number;
  avg_price: number;
  views: number;
}

async function getFullStatistics(): Promise<FullStatistics[]> {
  return await prisma.$queryRaw`
    select
      title,
      city_id,
      count(floor)::int as count,
      floor,
      avg(price / area)::int as avg_price_per_meter,
      avg(price)::int as avg_price,
      max(views)::int as views
    from
      flats
    where run_id = '${runId}'
    group by
      floor,
      title,
      city_id
    order by
      floor asc
  `;
}
