import { Extractor } from "./extractor";
import * as cheerio from "cheerio";
import { runId } from "../../config";
import { Element } from "cheerio";
import { Prisma } from "@prisma/client";

export class MyhomeAdvertExtractor extends Extractor<
  Partial<Prisma.AdvertCreateInput>
> {
  parse(data: string) {
    const $ = cheerio.load(data);
    return $(".statement-card")
      .toArray()
      .map((element: Element) => extractAdvert(element, $))
      .filter((advert) => !!advert.data_id);
  }
}

function extractAdvert(element: Element, $root: cheerio.CheerioAPI) {
  const $ = cheerio.load(element);
  return {
    run_id: runId,
    data_id: $(element).attr("data-product-id"),
    url: decodeURIComponent($(element).find("a.card-container").attr("href")),
    title: $(element).find(".card-title").text(),
    price: $(element).find(".item-price-usd").text(),
    address: $(element).find(".address").text(),
    area: $(element).find(".item-size").text(),
    floor: $(element).find('[data-tooltip="Этаж"] > span').text(),
    room: $(element).find('[data-tooltip="Количество комнат "] > span').text(),
    bedroom: $(element).find('[data-tooltip="Спальня"] > span').text(),
    description: $(element).find("p.description").text(),
    post_date: $(element).find(".statement-date").text(),
    city_id: $root(`form .developers_search_wrapper [name="cities"]`).attr('value'),
    created_date: new Date(),
  };
}
