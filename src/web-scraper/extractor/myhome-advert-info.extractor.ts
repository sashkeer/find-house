import { Extractor } from "./extractor";
import * as cheerio from "cheerio";
import { runId } from "../../config";
import { Prisma } from "@prisma/client";

// https://www.myhome.ge/en/pr/13475941/Newly-finished-apartment-for-sale-Parnavaz-Mepe-Ave.-4-rooms
export class MyhomeAdvertInfoExtractor extends Extractor<
  Partial<Prisma.AdvertCreateInput>
> {
  parse(data: string) {
    const $ = cheerio.load(data);
    const mapSelector = $("#map");
    const dataId = $(".id-container span").text().replace(": ", "");
    return [
      {
        run_id: runId,
        data_id: dataId,
        advert_info: {
          create: {
            data_id: dataId,
            description:
              $(".description .translated").text() ||
              $(".description .original").text(),
            phones: $("#PhoneModal .number").text(),
            geo_point: [
              mapSelector.attr("data-lat"),
              mapSelector.attr("data-lng"),
            ].filter(Boolean),
            owner_id: $(".see-all-statements").attr("href").split("=").pop(),
            amenities: $(".amenities-ul li")
              .toArray()
              .map((el) =>
                $(el)
                  .find(".d-block:not(.no)")
                  .text()
                  .replace(/\n/gi, " ")
                  .replace(/\t/gi, " ")
                  .replace(/ +/gi, " ")
                  .trim()
              )
              .filter(Boolean),
            photos: $(".detail-swiper-container .swiper-lazy")
              .toArray()
              .map((el) => $(el).attr("src"))
              .filter(Boolean),
            views: $(".views span").text(),
            breadcrumbs: $(".product-link-tree a")
              .toArray()
              .map((el) => $(el).text())
              .filter(Boolean),
          },
        },
      },
    ];
  }
}
