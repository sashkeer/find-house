import { Storage } from "./storage";
import { Prisma } from "@prisma/client";

export class MyhomeAdvertStorage extends Storage<Partial<Prisma.AdvertCreateManyInput>> {
  async save(data: Prisma.AdvertCreateManyInput[]) {
    await this.prisma.advert.createMany({
      data,
    });
  }
}
