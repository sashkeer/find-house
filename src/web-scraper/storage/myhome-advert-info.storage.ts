import { Storage } from "./storage";
import { Prisma } from "@prisma/client";

export class MyhomeAdvertInfoStorage extends Storage<Partial<Prisma.AdvertInfoCreateInput>> {
  async save(data: Prisma.AdvertInfoCreateInput[]) {
    await this.prisma.advertInfo.createMany({
      data,
    });
  }
}
