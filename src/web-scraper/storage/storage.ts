import { PrismaClient } from '@prisma/client';

export abstract class Storage<T> {
  constructor(protected prisma: PrismaClient) { }

  async save(data: T[]) {
    throw 'Save method not implemented';
  }
}