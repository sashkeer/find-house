export class Notifier<T> {
  public async notify(subject: string, text: string, attachments?: T[]) {}
}