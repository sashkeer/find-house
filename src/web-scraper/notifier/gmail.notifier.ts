import { Notifier } from "./notifier";
import { Transporter, createTransport } from "nodemailer";
import { notifierConfig } from "./config";
import { logActivity } from '../../utils';
import { google } from 'googleapis';
const OAuth2 = google.auth.OAuth2;

/**
 * How to
 * https://dev.to/chandrapantachhetri/sending-emails-securely-using-node-js-nodemailer-smtp-gmail-and-oauth2-g3a
 * */

export class GmailNotifier extends Notifier<{ name?: string; buffer?: Buffer; path?: string }> {
  private transporter: Transporter;

  private async init() {
    if (this.transporter) return;

    const oauth2Client = new OAuth2(
      process.env.CLIENT_ID,
      process.env.CLIENT_SECRET,
      'https://developers.google.com/oauthplayground'
    );

    oauth2Client.setCredentials({
      refresh_token: process.env.REFRESH_TOKEN
    });

    const accessToken: string = await new Promise((resolve, reject) => {
      oauth2Client.getAccessToken((err, token) => {
        if (err) {
          logActivity(err);
          reject("Failed to create access token :(");
        }
        resolve(token);
      });
    });

    this.transporter = createTransport({
      service: 'gmail',
      tls: {
        rejectUnauthorized: false
      },
      auth: {
        type: 'OAuth2',
        accessToken,
        user: process.env.GMAIL_USER,
        clientId: process.env.CLIENT_ID,
        clientSecret: process.env.CLIENT_SECRET,
        refreshToken: process.env.REFRESH_TOKEN,
      },
    });
  }

  async notify(
    subject: string,
    text: string,
    attachments?: { name?: string; buffer?: Buffer; path?: string }[]
  ): Promise<void> {
    await this.init();
    await new Promise((resolve, reject) => {
      this.transporter.sendMail(
        {
          from: process.env.GMAIL_USER,
          to: notifierConfig.mail,
          subject,
          text,
          ...(attachments
            ? {
              attachments: attachments.map((attachment) => ({
                content: attachment.buffer,
                filename: attachment.name,
              })),
            }
            : null),
        },
        function (error, info) {
          if (error) {
            console.log(error);
            reject(error);
          } else {
            console.log("Email sent: " + info.response);
            resolve(info);
          }
        }
      );
    });
  }
}
