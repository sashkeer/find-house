import { Scraper } from "./scraper";
import { Prisma } from "@prisma/client";
import * as cheerio from "cheerio";
import { logActivity } from "../../utils";

const joinQuery = (params: Record<string, any>) =>
  Object.keys(params)
    .map((key) => `${key}=${params[key]}`)
    .join("&");

const baseUrl = "https://www.myhome.ge/ru/s/Продается-квартира-Тбилиси";

const myhomeParams = {
  Keyword: "Тбилиси",
  AdTypeID: 1,
  PrTypeID: 1,
  OwnerTypeID: 1,
  regions: "687602533.687611312.687586034.689701920.689678147.688330506",
  districts:
    "2172993612.5469869.26445359.2022621279.738255266.798496409.190594087.411355289",
  cities: "1996871",
  GID: "1996871",
};

const realtyUrl = `${baseUrl}?${joinQuery(myhomeParams)}`;

export class MyhomeScraper extends Scraper<Prisma.AdvertCreateInput> {
  urls = [realtyUrl];

  async next(data: string): Promise<string | undefined> {
    const $ = cheerio.load(data);
    const hasNextPage = !$(".page-item.last-item.d-none").length;
    if (hasNextPage) {
      const currentPage = $(".page-item.number.normal-item.active").attr(
        "data-page"
      );
      if (Number.isInteger(+currentPage)) {
        const nextPage = +currentPage + 1;
        logActivity({ ...myhomeParams, Page: nextPage });
        return `${baseUrl}?${joinQuery({ ...myhomeParams, Page: nextPage })}`;
      } else {
        logActivity("No next page found");
      }
    }

    return;
  }
}
