import { Extractor } from "../extractor/extractor";
import { Crawler } from "../crawler/crawler";
import { Storage } from "../storage/storage";
import { logActivity, wait } from '../../utils';
import { NextPageProvider } from '../next-page/next-page.provider';

export class Scraper<T> {
  // public urls: string[] = [];

  private delay = 1000;

  constructor(
    private extractor: Extractor<T>,
    private crawler: Crawler,
    private storage: Storage<T>,
    private nextPageProvider: NextPageProvider = new NextPageProvider(),
  ) {}

  async start(urls: string[]) {
    logActivity("Start scraping");
    for (const url of urls) {
      await this.process(url);
    }
    await this.crawler.close();
    logActivity("Finish scraping");
  }

  private async process(url: string) {
    logActivity('scraping', url);
    const content = await this.crawler.getContent(url);
    const model = this.extractor.parse(content);
    await this.storage.save(model);

    await wait(this.delay);
    const nextUrl = await this.nextPageProvider.next(url, content);
    if (nextUrl) await this.process(nextUrl);
  }
}
