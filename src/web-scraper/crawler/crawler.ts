

export abstract class Crawler {
  async getContent(url: string): Promise<string> {
    throw 'Get content method not implemented';
  };

  async close() {
    throw 'Close method not implemented';
  }
}