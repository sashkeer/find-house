import { PageController } from './page.controller';
import * as puppeteer from 'puppeteer';

export class MyhomePageController extends PageController {

  async manipulatePage(page: puppeteer.Page) {
    try {
      await page.click('.phone-btn')
      await page.waitForSelector('#PhoneModal', { timeout: 5000 })
    } catch (e) {
      console.log('No phone number');
    }

    try {
      await page.waitForSelector('#map', { timeout: 5000 })
    } catch (e) {
      console.log('No phone number');
    }
  }

}