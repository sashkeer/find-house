import { Crawler } from "./crawler";
import * as puppeteer from "puppeteer";
import { logActivity } from "../../utils";
import { Browser } from "puppeteer";
import { PageController } from "./page.controller/page.controller";

export class PuppeteerCrawler extends Crawler {
  private browser: Browser;

  constructor(private pageController: PageController = new PageController()) {
    super();
  }

  private async getBrowser() {
    if (this.browser) return this.browser;
    this.browser = await puppeteer.launch();
    return this.browser;
  }

  async getContent(url: string): Promise<string> {
    this.browser = await this.getBrowser();
    const page = await this.browser.newPage();
    logActivity("Opening myhome page");
    await page.goto(url);
    await this.pageController.manipulatePage(page);
    logActivity("Waiting for cards");
    const content = await page.content();
    await page.close();
    return content;
  }

  async close() {
    await this.browser.close();
  }
}
