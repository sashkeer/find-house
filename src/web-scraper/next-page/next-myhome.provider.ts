import * as cheerio from 'cheerio';
import { logActivity } from '../../utils';
import { NextPageProvider } from './next-page.provider';
const { URL } = require('url');

export class NextMyhomeProvider extends NextPageProvider {
  async next(url: string, data: string): Promise<string | undefined> {
    const $ = cheerio.load(data);
    const hasNextPage = !$(".page-item.last-item.d-none").length;
    if (hasNextPage) {
      const currentPage = $(".page-item.number.normal-item.active").attr(
        "data-page"
      );
      if (Number.isInteger(+currentPage)) {
        const nextPage = +currentPage + 1;
        const parsedUrl = new URL(url);
        parsedUrl.searchParams.set('Page', nextPage)
        return parsedUrl.toString();
      } else {
        logActivity("No next page found");
      }
    }

    return;
  }
}