-- CreateEnum
CREATE TYPE "CostOfLiving" AS ENUM ('LOW', 'MEDIUM', 'HIGH');

-- CreateTable
CREATE TABLE "District" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "has_metro" BOOLEAN NOT NULL,
    "cost_of_living" "CostOfLiving" NOT NULL DEFAULT 'MEDIUM',
    "facilities" BOOLEAN NOT NULL,
    "created_date" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "District_pkey" PRIMARY KEY ("id")
);
