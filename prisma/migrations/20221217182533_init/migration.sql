/*
  Warnings:

  - You are about to drop the column `city_id` on the `AdvertInfo` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Advert" ADD COLUMN     "city_id" TEXT;

-- AlterTable
ALTER TABLE "AdvertInfo" DROP COLUMN "city_id";
