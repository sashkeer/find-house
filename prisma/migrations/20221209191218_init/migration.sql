-- CreateTable
CREATE TABLE "Advert" (
    "id" SERIAL NOT NULL,
    "run_id" TEXT NOT NULL,
    "data_id" TEXT NOT NULL,
    "url" TEXT,
    "title" TEXT,
    "price" TEXT,
    "address" TEXT,
    "area" TEXT,
    "floor" TEXT,
    "room" TEXT,
    "bedroom" TEXT,
    "description" TEXT,
    "created_date" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "advert_info_id" INTEGER,

    CONSTRAINT "Advert_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "AdvertInfo" (
    "id" SERIAL NOT NULL,
    "run_id" TEXT NOT NULL,
    "data_id" TEXT NOT NULL,
    "description" TEXT,
    "phones" TEXT,
    "geo_point" TEXT,
    "point" TEXT,
    "post_date" TEXT,
    "owner_id" TEXT,
    "created_date" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "AdvertInfo_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Advert_run_id_data_id_key" ON "Advert"("run_id", "data_id");

-- CreateIndex
CREATE UNIQUE INDEX "AdvertInfo_run_id_data_id_key" ON "AdvertInfo"("run_id", "data_id");

-- AddForeignKey
ALTER TABLE "Advert" ADD CONSTRAINT "Advert_advert_info_id_fkey" FOREIGN KEY ("advert_info_id") REFERENCES "AdvertInfo"("id") ON DELETE SET NULL ON UPDATE CASCADE;
