-- CreateTable
CREATE TABLE "SearchParams" (
    "id" SERIAL NOT NULL,
    "key" TEXT NOT NULL,
    "parameters" JSONB NOT NULL,
    "active" BOOLEAN NOT NULL DEFAULT true,

    CONSTRAINT "SearchParams_pkey" PRIMARY KEY ("id")
);
