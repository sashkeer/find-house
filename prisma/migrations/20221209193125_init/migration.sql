/*
  Warnings:

  - You are about to drop the column `point` on the `AdvertInfo` table. All the data in the column will be lost.
  - You are about to drop the column `post_date` on the `AdvertInfo` table. All the data in the column will be lost.
  - The `geo_point` column on the `AdvertInfo` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Advert" ADD COLUMN     "post_date" TEXT;

-- AlterTable
ALTER TABLE "AdvertInfo" DROP COLUMN "point",
DROP COLUMN "post_date",
ADD COLUMN     "amenities" TEXT[],
ADD COLUMN     "photos" TEXT[],
DROP COLUMN "geo_point",
ADD COLUMN     "geo_point" TEXT[];
