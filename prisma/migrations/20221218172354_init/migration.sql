/*
  Warnings:

  - You are about to drop the column `advert_info_id` on the `Advert` table. All the data in the column will be lost.
  - The primary key for the `AdvertInfo` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `AdvertInfo` table. All the data in the column will be lost.
  - You are about to drop the column `run_id` on the `AdvertInfo` table. All the data in the column will be lost.

*/
-- DropForeignKey
ALTER TABLE "Advert" DROP CONSTRAINT "Advert_advert_info_id_fkey";

-- DropIndex
DROP INDEX "AdvertInfo_run_id_data_id_key";

-- AlterTable
ALTER TABLE "Advert" DROP COLUMN "advert_info_id";

-- AlterTable
ALTER TABLE "AdvertInfo" DROP CONSTRAINT "AdvertInfo_pkey",
DROP COLUMN "id",
DROP COLUMN "run_id",
ADD CONSTRAINT "AdvertInfo_pkey" PRIMARY KEY ("data_id");
