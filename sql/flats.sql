-- public.flats source

CREATE OR REPLACE VIEW public.flats
AS SELECT replace(a.price, ','::text, ''::text)::integer AS price,
    a.title,
    replace(a.floor, 'Этаж '::text, ''::text)::integer AS floor,
    replace(a.room, 'Комн. '::text, ''::text)::integer AS room,
    a.url,
    ai.description,
    ai.phones,
    ai.photos,
    ai.amenities,
    ai.views::integer AS views,
    ai.created_date,
    a.post_date,
    a.data_id,
    a.city_id,
    ai.breadcrumbs,
    regexp_replace(a.area, '[^\d]'::text, ''::text, 'gi'::text)::integer / 100 AS area,
    to_date((EXTRACT(year FROM a.created_date) || myhomemonth(a.post_date)) || myhomeday(a.post_date), 'YYYYMonthdd'::text) AS publish_date,
    ai.description ~* 'срочно'::text OR ai.description ~* 'быстро'::text AS hasten,
    (( SELECT count(a_1.data_id) AS count
           FROM "Advert" a_1
          WHERE a_1.data_id = a.data_id)) < 2 AS is_new,
    ai.breadcrumbs[2] AS city,
    ai.breadcrumbs[3] AS district,
    (ai.description ~* 'скидка' or ai.description ~* 'скидку') AS discount,
    a.run_id
   FROM "Advert" a
     LEFT JOIN "AdvertInfo" ai ON a.data_id = ai.data_id;